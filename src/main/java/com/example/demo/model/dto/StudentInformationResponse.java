package com.example.demo.model.dto;

import com.example.demo.model.entities.StudentSpreads;

import java.util.List;

public class StudentInformationResponse {
    public String studentName;
    public String departmentName;
}
