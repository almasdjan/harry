package com.example.demo.model.dto;

public class SpreadingResponse {
    public String department;

    public static SpreadingResponse of(String department) {
        SpreadingResponse spreadingResponse = new SpreadingResponse();
        spreadingResponse.department = department;
        return spreadingResponse;
    }
}
