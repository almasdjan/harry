package com.example.demo.model.entities;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="character")
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "character")
    private Set<CharacterSpread> characterSpread;



    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<CharacterSpread> getCharacterSpread() {
        return characterSpread;
    }

    public void setCharacterSpread(Set<CharacterSpread> characterSpread) {
        this.characterSpread = characterSpread;
    }
}
