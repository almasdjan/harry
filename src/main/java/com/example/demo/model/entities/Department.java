package com.example.demo.model.entities;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="department")
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "department")
    Set<BloodLineSpread> bloodLineSpread;

    @OneToMany(mappedBy = "department")
    Set<WandTypeSpread> wandTypeSpread;

    @OneToMany(mappedBy = "department")
    Set<CharacterSpread> characterSpread;

    private String departmentName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
    public static void main() {
        throw new RuntimeException("this is runtime exception");
    }




}
