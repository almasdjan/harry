package com.example.demo.model.entities;

import javax.persistence.*;

@Entity
@Table(name="wand_type_spread")
public class WandTypeSpread {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name="wand_type_id")
    WandType wandType;

    @ManyToOne
    @JoinColumn(name="department_id")
    Department department;

    Integer percent;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPercent() {
        return percent;
    }

    public void setPercent(Integer percent) {
        this.percent = percent;
    }


    public WandType getWandType() {
        return wandType;
    }

    public void setWandType(WandType wandType) {
        this.wandType = wandType;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
}
