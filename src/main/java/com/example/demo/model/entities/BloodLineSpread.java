package com.example.demo.model.entities;

import javax.persistence.*;


@Entity
@Table(name="blood_line_spread")
public class BloodLineSpread{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name="blood_line_id")
    BloodLine bloodLine;

    @ManyToOne
    @JoinColumn(name="department_id")
    Department department;


//    Long bloodLineId;
//    Long departmentId;

    Integer percent;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BloodLine getBloodLine() {
        return bloodLine;
    }

    public void setBloodLine(BloodLine bloodLine) {
        this.bloodLine = bloodLine;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

//    public Long getBloodLineId() {
//        return bloodLineId;
//    }
//
//    public void setBloodLineId(Long bloodLineId) {
//        this.bloodLineId = bloodLineId;
//    }
//
//    public Long getDepartmentId() {
//        return departmentId;
//    }
//
//    public void setDepartmentId(Long departmentId) {
//        this.departmentId = departmentId;
//    }

    public Integer getPercent() {
        return percent;
    }

    public void setPercent(Integer percent) {
        this.percent = percent;
    }
}
