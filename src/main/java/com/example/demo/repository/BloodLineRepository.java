package com.example.demo.repository;

import com.example.demo.model.entities.BloodLine;
import com.example.demo.model.entities.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BloodLineRepository extends JpaRepository<BloodLine, Long> {
    BloodLine findByBloodLineName(String bloodLineName);
    Optional<BloodLine> findById(Long id);
}
