package com.example.demo.repository;


import com.example.demo.model.entities.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import com.example.demo.model.entities.Student;
import java.util.Optional;

public interface CharacterRepository extends JpaRepository<Character,Long> {
    Character findByName(String name);
    Optional<Character> findById(Long id);

}
