package com.example.demo.repository;

import com.example.demo.model.entities.Student;
import com.example.demo.model.entities.StudentSpreads;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentSpreadsRepository extends JpaRepository<StudentSpreads, Long> {
    boolean existsByStudent(Student student);
}
