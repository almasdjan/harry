package com.example.demo.service;

import com.example.demo.model.entities.Student;
import com.example.demo.model.entities.Character;
import com.example.demo.repository.CharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CharacterService{
    @Autowired
    CharacterRepository characterRepository;
    public Character findById(Long id) {

        return characterRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Cannot find character by Id: "+id));
    }


}

