package com.example.demo.service;

import com.example.demo.model.entities.Student;
import com.example.demo.model.entities.WandType;
import com.example.demo.repository.WandTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MagicWantService {

    @Autowired
    private WandTypeRepository wandTypeRepository;
    public WandType findById(Long id){
        return wandTypeRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Cannot find wandtype by Id: "+id));
    }
}
