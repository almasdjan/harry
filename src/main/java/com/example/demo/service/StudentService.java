package com.example.demo.service;

import com.example.demo.model.entities.Student;
import com.example.demo.repository.StudentRepository;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentService {
    @Autowired
    StudentRepository studentRepository;

    public List<Student> getAll() {
        return studentRepository.findAll();
    }

    public void save(Student students) {
        studentRepository.save(students);
    }

    public void deleteById(Long id) {
        studentRepository.deleteById(id);
    }

    public Student getStudentByName(String fullName) {
        Student student = studentRepository.findByFullName(fullName);
        if(student == null) {
            throw new RuntimeException("Student by name : " + fullName + " not found!");
        }
        return student;
    }

   


}
