
insert into bloodline(blood_line_name) values('Potters');
insert into bloodline(blood_line_name) values('Malfoys');
insert into bloodline(blood_line_name) values('Lovegoods');
insert into bloodline(blood_line_name) values('Diggeries');

insert into character(name) values ('Brave');
insert into character(name) values ('Cunning');
insert into character(name) values ('Intelligent');
insert into character(name) values ('Hardworking');

insert into department(department_name) values ('Gryffindor');
insert into department(department_name) values ('Slytherin');
insert into department(department_name) values ('Ravenclaw');
insert into department(department_name) values ('Hufflepuff');

insert into wand_type(name) values ('Unicorn air');
insert into wand_type(name) values ('Dragon heartstring');
insert into wand_type(name) values ('Phoenix feather');
insert into wand_type(name) values ('Basilisk horn');

insert into blood_line_spread(blood_line_id,department_id,percent) values (1,1,60);
insert into blood_line_spread(blood_line_id,department_id,percent)  values (1,2,10);
insert into blood_line_spread(blood_line_id,department_id,percent)  values (1,3,10);
insert into blood_line_spread(blood_line_id,department_id,percent)  values (1,4,10);
insert into blood_line_spread(blood_line_id,department_id,percent)  values (2,1,10);
insert into blood_line_spread(blood_line_id,department_id,percent)  values (2,1,60);
insert into blood_line_spread(blood_line_id,department_id,percent)  values (2,3,10);
insert into blood_line_spread(blood_line_id,department_id,percent)  values (2,4,10);
insert into blood_line_spread(blood_line_id,department_id,percent)  values (3,1,10);
insert into blood_line_spread(blood_line_id,department_id,percent) values (3,2,10);
insert into blood_line_spread(blood_line_id,department_id,percent)  values (3,3,60);
insert into blood_line_spread(blood_line_id,department_id,percent)  values (3,4,10);
insert into blood_line_spread(blood_line_id,department_id,percent)  values (4,1,10);
insert into blood_line_spread(blood_line_id,department_id,percent)  values (4,2,10);
insert into blood_line_spread(blood_line_id,department_id,percent)  values (4,3,10);
insert into blood_line_spread(blood_line_id,department_id,percent)  values (4,4,60);

insert into character_spread(character_id,department_id,percent)  values (1,1,40);
insert into character_spread(character_id,department_id,percent) values (1,2,40);
insert into character_spread(character_id,department_id,percent) values (2,1,40);
insert into character_spread(character_id,department_id,percent) values (2,2,40);
insert into character_spread(character_id,department_id,percent) values (3,1,20);
insert into character_spread(character_id,department_id,percent) values (3,2,20);
insert into character_spread(character_id,department_id,percent) values (3,2,50);
insert into character_spread(character_id,department_id,percent) values (4,1,15);
insert into character_spread(character_id,department_id,percent) values (4,2,5);
insert into character_spread(character_id,department_id,percent) values (4,3,25);
insert into character_spread(character_id,department_id,percent) values (4,3,40);

insert into wand_type_spread(wand_type_id,department_id,percent) values (1,1,15);
insert into wand_type_spread(wand_type_id,department_id,percent) values (1,2,20);
insert into wand_type_spread(wand_type_id,department_id,percent) values (1,3,30);
insert into wand_type_spread(wand_type_id,department_id,percent) values (1,4,35);
insert into wand_type_spread(wand_type_id,department_id,percent) values (2,1,20);
insert into wand_type_spread(wand_type_id,department_id,percent) values (2,2,15);
insert into wand_type_spread(wand_type_id,department_id,percent) values (2,3,40);
insert into wand_type_spread(wand_type_id,department_id,percent) values (2,4,20);
insert into wand_type_spread(wand_type_id,department_id,percent) values (3,1,40);
insert into wand_type_spread(wand_type_id,department_id,percent) values (3,2,30);
insert into wand_type_spread(wand_type_id,department_id,percent) values (3,3,10);
insert into wand_type_spread(wand_type_id,department_id,percent) values (3,4,10);
insert into wand_type_spread(wand_type_id,department_id,percent) values (4,1,45);
insert into wand_type_spread(wand_type_id,department_id,percent) values (4,2,15);
insert into wand_type_spread(wand_type_id,department_id,percent) values (4,3,10);
insert into wand_type_spread(wand_type_id,department_id,percent) values (4,4,10);

insert into students(full_name, character_id, wand_type_id, blood_line_id) values ('Harry Potter', 1, 3, 1);
insert into students(full_name, character_id, wand_type_id, blood_line_id) values ('Draco Malfoy', 2, 2, 2);
insert into students(full_name, character_id, wand_type_id, blood_line_id) values ('Cedric Diggery', 4, 1, 4);
insert into students(full_name, character_id, wand_type_id, blood_line_id) values ('Luna Lovegood', 3, 4, 3);












