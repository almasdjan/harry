CREATE TABLE student_spreads
(
    id         bigserial primary key,
    full_name  varchar(255),
    department varchar(255)
);

CREATE TABlE students
(
    id bigserial primary key ,
    full_name varchar(255)
);

ALTER TABLE student_spreads DROP COLUMN full_name;
ALTER TABLE student_spreads
    ADD COLUMN student_id bigint,
    ADD FOREIGN KEY(student_id) REFERENCES students(id);

ALTER TABLE students
    ADD COLUMN blood_line varchar(255);

ALTER TABLE students
    ADD COLUMN wand_type varchar(255);


ALTER TABLE students
    ADD COLUMN character varchar(255);


CREATE TABlE bloodline
(
    id bigserial primary key ,
    bloodline_name varchar(255),
    gryffindorPercent numeric,
    slyzerinePercent numeric,
    hafflepuffPercent numeric,
    ravenclawPercent numeric


);
drop table bloodline;

create table bloodline(
                          id bigserial primary key ,
                          blood_line_name varchar(200)
);

create table department(
                           id bigserial primary key ,
                           department_name varchar(200)
);

create table blood_line_spread(
                                  id bigserial primary key ,
                                  blood_line_id bigserial,
                                  department_id bigserial,
                                  foreign key(blood_line_id) references bloodline(id),
                                  foreign key (department_id) references department(id)
);

ALTER table students
drop column blood_line;

alter table students
    add column bloodline_id bigint,
    add foreign key (bloodline_id) references bloodline(id);

select * from students s JOIN bloodline b ON s.bloodline_id=b.id;

ALTER TABLE students
    RENAME COLUMN bloodline_id TO blood_line_id;

alter table blood_line_spread
    add column percent bigint;





create table wand_type(
                          id bigserial primary key,
                          name varchar(255)

);




alter table students
    add column wand_type_id int ,
    add foreign key(wand_type_id) references wand_type(id);

create table wand_type_spread(
                                 id bigserial primary key ,
                                 wand_type_id int references wand_type(id),
                                 department_id int references department(id),
                                 percent numeric
);


create table character(
                          id bigserial primary key,
                          name varchar(255)
);

create table character_spread(
                                 id bigserial primary key ,
                                 character_id bigint references character(id),
                                 department_id bigint references department(id),
                                 percent numeric
);
alter table students
drop column character;



alter table students
    add column character_id int ,
    add foreign key(character_id) references character(id);


alter table student_spreads
drop column department;

alter table student_spreads
    add column department_id bigint references department(id);

