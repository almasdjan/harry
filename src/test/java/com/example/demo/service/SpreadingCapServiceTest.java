//package com.example.demo.service;
//
//import com.example.demo.model.*;
//import com.example.demo.model.entities.StudentSpreads;
//import com.example.demo.model.enums.BloodLine;
//import com.example.demo.model.enums.Departments;
//import com.example.demo.model.enums.WandType;
//import com.example.demo.repository.StudentSpreadsRepository;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.Mock;
//import org.mockito.junit.jupiter.MockitoExtension;
//
//import static org.mockito.Mockito.verify;
//import static org.mockito.Mockito.when;
//
//@ExtendWith(MockitoExtension.class)
//class SpreadingCapServiceTest {
//
//    private SpreadingCapService spreadingCapService;
//
//    @Mock
//    BloodLineService bloodLineService;
//
//    @Mock
//    DepartmentService departmentService;
//
//    @Mock
//    CharacterService characterService;
//
//    @Mock
//    MagicWantService magicWantService;
//
//    @Mock
//    StudentSpreadsRepository studentSpreadsRepository;
//
//    @BeforeEach
//    void setUp() {
//        spreadingCapService = new SpreadingCapService(bloodLineService, magicWantService, characterService, departmentService, studentSpreadsRepository);
//    }
//
//    @Test
//    void spread() {
//        //given
//        String fullName = "Sedric";
//        DepartmentExpectations expectationByCharacteristics = new DepartmentExpectations();
//        DepartmentExpectations departmentExpectations = new DepartmentExpectations();
//        departmentExpectations.hafflepuffPercent = 80;
//        departmentExpectations.gryffindorPercent = 20;
//
//        StudentSpreads studentSpreads = new StudentSpreads();
//        studentSpreads.fullName = fullName;
//        studentSpreads.department = Departments.HAFFLEPUFF;
//
//        //when
//        when(bloodLineService.getBloodLine(fullName)).thenReturn(BloodLine.DIGGERY);
//        when(magicWantService.getWandType(fullName)).thenReturn(WandType.PHEONIX_FEATHER);
//        when(characterService.getCharacteristics(fullName)).thenReturn(expectationByCharacteristics);
//        when(departmentService.getExpectationByWand(WandType.PHEONIX_FEATHER)).thenReturn(expectationByCharacteristics);
//        when(departmentService.getExpectationByBloodLine(BloodLine.DIGGERY)).thenReturn(expectationByCharacteristics);
//
//        //then
//        spreadingCapService.spread(fullName, null);
//        verify(studentSpreadsRepository).addStudent(studentSpreads);
//    }
//}